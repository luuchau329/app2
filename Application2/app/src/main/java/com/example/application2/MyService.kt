package com.example.application2

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import kotlinx.coroutines.*


class MyService : Service() {
    val NOTIF_ID = 2
    val NOTIF_CHANNEL_ID = "CHANNEL_ID2"

    lateinit var job: Job
    val timeReStream: Long =  15 * 1000


    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            startForeroundService()
        else
            startForeground(2, Notification())

        var rec = MyBroadcastReceiver()
        val intentFilter = IntentFilter("com.example.myapplication.RestartActivity")
        if (intentFilter != null) {
            registerReceiver(rec, intentFilter)
        }

        initialJob()

        return START_STICKY

    }


    fun startForeroundService() {

        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel("my_service2", NOTIF_CHANNEL_ID)
            } else {
                // If earlier version channel ID is not used
                // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                ""
            }
        startForeground(
            NOTIF_ID, NotificationCompat.Builder(
                this,
                channelId
            )
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Service2 is running background")
                .build()
        )


    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String{
        val chan = NotificationChannel(channelId,
            channelName, NotificationManager.IMPORTANCE_NONE)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    override fun onDestroy() {
        super.onDestroy()

    }


    override fun onTaskRemoved(rootIntent: Intent?) {

        Intent(this, MainActivity::class.java).apply {
            this.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(this)
        }

    }


    private fun initialJob(){
        job = CoroutineScope(Dispatchers.IO).launch {
            while (true){

                startNewActivity(this@MyService,"com.example.myapplication")
                Log.d("DEBUGAPP", " initialJob2 ")
                startNewService()
                delay(timeReStream)
            }
        }


    }
    private fun cancelJob(){
        job.cancel()
        Log.e("TAG","Job is Cancel")
    }

    fun startNewActivity(context: Context, packageName: String) {
        var intent: Intent = context.getPackageManager().getLaunchIntentForPackage(packageName)!!
        if (intent != null) {
            // We found the activity now start the activity
          //  intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        } else {
            // Bring user to the market or let them choose an app?
            intent = Intent(Intent.ACTION_VIEW)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.data = Uri.parse("market://details?id=$packageName")
            context.startActivity(intent)
        }
    }

    fun startNewService() {
        val intent = Intent()
//        intent.setClassName("com.example.application2","com.example.application2.MyService")

        intent.component =
            ComponentName("com.example.myapplication", "com.example.myapplication.MyService")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent)
        } else {
            startService(intent)

        }

    }


}