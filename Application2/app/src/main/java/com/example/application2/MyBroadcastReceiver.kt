package com.example.application2

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class MyBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent?) {
        if (p1?.action == "com.example.myapplication.RestartActivity") {
            Log.d("DEBUGAPP", "App 2 onReceive: ")
        }
    }
}